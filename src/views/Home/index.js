import React, { Component } from "react";
import "./Home.css";

import Header from "../../components/Header";
import Gallery from "../../components/Gallery";
import Alert from "../../components/Alert";
import Lightbox from "../../components/Lightbox";

import helpers from "../../helpers";

class Home extends Component {
  constructor() {
    super();

    this.state = {
      per_page: 20,
      page: 1,
      photos: [],
      total: 0,
      alert: {
        show: false,
        title: "",
        body: "",
        type: "default"
      },
      more_disabled: false,
      lightbox: {
        show: false
      }
    };

    this.closeAlert = this.closeAlert.bind(this);
    this.updateAlert = this.updateAlert.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.getFlickerPhotos = this.getFlickerPhotos.bind(this);
    this.loadMorePhotos = this.loadMorePhotos.bind(this);
    this.showLightbox = this.showLightbox.bind(this);
  }

  componentDidMount() {
    const params = {
      per_page: this.state.per_page,
      page: this.state.page
    };

    this.getFlickerPhotos(helpers.getFlickrUrl(params));
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  getFlickerPhotos(flickr_url) {
    this.setState({
      more_disabled: true
    });

    fetch(flickr_url)
      .then(response => {
        if (response.status >= 400) {
          const alert = {
            show: true,
            title: "Error",
            body: "Bad response from server",
            type: "error"
          };

          this.setState({
            more_disabled: false,
            alert
          });
        }
        return response.json();
      })
      .then(data => {
        const photos = [...this.state.photos, ...data.photos.photo];

        this.setState({
          more_disabled: false,
          total: data.photos.total,
          photos
        });
      });
  }

  updateAlert(alert) {
    this.setState({
      alert
    });
  }

  closeAlert() {
    let alert = { ...this.state.closeAlert };
    alert.show = false;

    this.setState({
      alert
    });
  }

  handleScroll() {
    const windowHeight =
      "innerHeight" in window
        ? window.innerHeight
        : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    );
    const windowBottom = windowHeight + window.pageYOffset;

    if (windowBottom >= docHeight) this.loadMorePhotos();
  }

  loadMorePhotos() {
    const params = {
      per_page: this.state.per_page,
      page: this.state.page++
    };

    this.getFlickerPhotos(helpers.getFlickrUrl(params));

    this.setState({
      page: this.state.page++
    });
  }

  showLightbox(card) {
    let lightbox = {
      show: !this.state.lightbox.show,
      card
    };

    this.setState({
      lightbox
    });
  }

  render() {
    return (
      <div className="home">
        <Header />
        <Gallery
          photos={this.state.photos}
          updateAlert={this.updateAlert}
          showLightbox={this.showLightbox}
        />
        <Alert alert={this.state.alert} close={this.closeAlert} />
        <input
          disabled={this.state.more_disabled}
          className="more"
          type="button"
          value="LOAD MORE PHOTOS"
          onClick={this.loadMorePhotos}
        />
        <Lightbox
          lightbox={this.state.lightbox}
          showLightbox={this.showLightbox}
        />
      </div>
    );
  }
}

export default Home;
