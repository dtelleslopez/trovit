import React, { Component } from "react";
import "./Card.css";

import helpers from "../../helpers";

import photo_default from "../../../dist/assets/default.png";

class Card extends Component {
  constructor() {
    super();

    this.state = {
      photo_source: photo_default,
      photo_url: photo_default,
      sizes: [],
      owner: {}
    };
  }

  componentWillMount() {
    const photo_url = helpers.getPhotoFromUrl({
      photo_id: this.props.card.id
    });

    const owner_url = helpers.getFlickrOwner({
      user_id: this.props.card.owner
    });

    fetch(photo_url)
      .then(response => {
        if (response.status >= 400) {
          const alert = {
            show: true,
            title: "Error",
            body: "Bad response from server",
            type: "error"
          };

          this.props.updateAlert(alert);
        }
        return response.json();
      })
      .then(data => {
        const sizes = [...data.sizes.size.reverse()];

        for (const size in sizes) {
          if (parseInt(sizes[size].width) <= 600) {
            this.setState({
              photo_source: sizes[size].source,
              photo_url: sizes[size].url,
              sizes
            });

            break;
          }
        }

        fetch(owner_url)
          .then(response => {
            if (response.status >= 400) {
              const alert = {
                show: true,
                title: "Error",
                body: "Bad response from server",
                type: "error"
              };

              this.props.updateAlert(alert);
            }
            return response.json();
          })
          .then(data => {
            this.setState({
              owner: data.person
            });
          });
      });
  }

  render() {
    const card = {
      ...this.props.card,
      ...{ sizes: this.state.sizes, owner: this.state.owner }
    };

    return (
      <div className="card">
        <div
          className="photo"
          style={{ backgroundImage: `url(${this.state.photo_source})` }}
          onClick={() => this.props.showLightbox(card)}
        />
        <div className="info">
          <div className="title">
            <span>{this.props.card.title}</span>
          </div>
          <div className="owner">
            <i>
              <span>
                {this.state.owner.username
                  ? `by ${this.state.owner.username._content}`
                  : null}
              </span>
            </i>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
