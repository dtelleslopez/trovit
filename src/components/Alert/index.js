import React, { Component } from "react";
import "./Alert.css";

class Alert extends Component {
  render() {
    if (!this.props.alert.show) return null;

    return (
      <div className={`alert ${this.props.alert.type}`}>
        <span className="close" onClick={() => this.props.close()}>
          X
        </span>
        <span className="title">{this.props.alert.title}</span>
        <span className="body">{this.props.alert.body}</span>
      </div>
    );
  }
}

export default Alert;
