import React, { Component } from "react";
import "./Lightbox.css";

class Lightbox extends Component {
  render() {
    if (!this.props.lightbox.show) return null;

    return (
      <div className="lightbox">
        <span className="close" onClick={() => this.props.showLightbox()}>
          X
        </span>
        <img src={this.props.lightbox.card.sizes[0].source} />
        <h3>{this.props.lightbox.card.title}</h3>
        <span>{this.props.lightbox.card.owner.username._content}</span>
        <div className="sizes">
          {this.props.lightbox.card.sizes.map((size, key) => {
            return (
              <a className="size" href={size.url} target="_blanck" key={key}>
                {size.label}
              </a>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Lightbox;
