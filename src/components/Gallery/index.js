import React, { Component } from "react";
import "./Gallery.css";

import Card from "../Card";

class Gallery extends Component {
  render() {
    return (
      <div className="gallery">
        {this.props.photos.map((photo, key) => {
          return (
            <Card
              key={key}
              card={photo}
              updateAlert={this.props.updateAlert}
              showLightbox={this.props.showLightbox}
            />
          );
        })}
      </div>
    );
  }
}

export default Gallery;
