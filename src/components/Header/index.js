import React, { Component } from "react";
import "./Header.css";

import logo from "../../../dist/assets/logo.png";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <img src={logo} width="40" alt="Trovit" /> <h1>Trovit</h1>
      </div>
    );
  }
}

export default Header;
