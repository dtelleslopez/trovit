module.exports = params => {
  if (!params || !params.photo_id) return;
  const flickr_url = `https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=${API_KEY}&photo_id=${
    params.photo_id
  }&format=json&nojsoncallback=1`;
  return flickr_url;
};
