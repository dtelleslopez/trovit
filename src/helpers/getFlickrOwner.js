module.exports = params => {
  if (!params || !params.user_id) return;
  const flickr_url = `https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=${API_KEY}&user_id=${
    params.user_id
  }&format=json&nojsoncallback=1`;
  return flickr_url;
};
