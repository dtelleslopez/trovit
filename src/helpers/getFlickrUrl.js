module.exports = params => {
  if (!params || !params.per_page || !params.page) return;
  const flickr_url = `https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=${API_KEY}&per_page=${
    params.per_page
  }&page=${params.page}&safe_search=1&format=json&nojsoncallback=1`;
  return flickr_url;
};
