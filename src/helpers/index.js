import getFlickrUrl from "./getFlickrUrl";
import getPhotoFromUrl from "./getPhotoFromUrl";
import getFlickrOwner from "./getFlickrOwner";

module.exports = {
  getFlickrUrl,
  getPhotoFromUrl,
  getFlickrOwner
};
