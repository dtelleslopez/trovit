const chai = require("chai");
const expect = require("chai").expect;
const flickr_url = `https://api.flickr.com`;

chai.use(require("chai-http"));

describe("Flickr", () => {
  it("should return object of photos", () => {
    return chai
      .request(flickr_url)
      .get(
        `/services/rest/?method=flickr.photos.getRecent&api_key=${
          process.env.API_KEY
        }&per_page=10&page=1&safe_search=1&format=json&nojsoncallback=1`
      )
      .then(res => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an("object");
      });
  });
});
