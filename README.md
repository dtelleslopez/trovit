# README #

### Tech

  This test uses a number of open source projects to work properly:

* React - A JavaScript library for building user interfaces
* Webpack - Module bundler and build tool
* Babel - Enables you writing your code in ES6 (ES2015)
* Hot Loader - Changes will apply without reloading the entire page
* CSS Loader - Compile and/or transform CSS resources bundled as a javascript module
* File Loader - Compile and/or transform Images resources bundled as a javascript module
* Node - Evented I/O for the backend

## Installation

  Node.js is required. Before installing, [download and install Node.js](https://nodejs.org/en/download/).
  Install the dependencies and start the development server, the project will run at http://localhost:8080/

```bash
$ npm install
$ API_KEY=<key> npm run dev
```

## Tests

  To run the test suite, first install the dependencies:

```bash
$ npm install
$ API_KEY=<key> npm test
```

### Notes

* The maximum width allowed in the preview gallery is 600px
* Each page has 20 images
* Easy auto scroll pagination
* Mobile friendly
* Not ready for production, needs express or similar to serve files